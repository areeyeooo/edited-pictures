       IDENTIFICATION DIVISION. 
       PROGRAM-ID.  EDIT2.
       AUTHOR. AREEYEO.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  STARS     PIC *****.
       01  NUM-OF-STARS PIC 9.
       PROCEDURE DIVISION .
       BEGIN.
           PERFORM VARYING NUM-OF-STARS FROM 0 BY 1 UNTIL 
               NUM-OF-STARS > 5
               COMPUTE STARS = 10 ** ( 4 - NUM-OF-STARS)
               INSPECT STARS CONVERTING "10" TO  SPACES 
               DISPLAY NUM-OF-STARS " = " STARS
           END-PERFORM
           STOP RUN 
           
           .
